package com.company;

/**
 * Created by User on 2/28/2017.
 */
public interface PaymentOptionsOC {
    public void onlinePayment();
    public void onDeliveryPayment();
}
